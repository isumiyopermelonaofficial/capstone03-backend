// [SECTION] Dependencies and Modules
const User =  require('../models/User.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

dotenv.config();
const salt = Number(process.env.SALT);

// Register New User
module.exports.registerUser = (req,res) => {
	let userFirstName = req.body.firstName;
	let userLastName = req.body.lastName;
	let userEmail = req.body.email;
	let userPassword = req.body.password;

	let newUser = new User({
		firstName:userFirstName,
		lastName:userLastName,
		email:userEmail,
		password:bcrypt.hashSync(userPassword,salt)
	});

	return User.findOne({email:userEmail}).then(foundUser => {
		if(foundUser){
			return res.send({message: `${userEmail} Already Registered`});
		} else {
			return newUser.save().then((user,error) => {
				if(error){
					return res.send({message: "Error: Unable to Register User"});
				} else {
					return res.send({message: `User ${userEmail} Successfully Registered`});
				}
			});
		}
	});
};

// Update User Information
module.exports.updateUser = (req,res) => {
	let userId = req.params.id;
	let details = req.body;

	let uFirstName = details.firstName;
	let uLastName = details.lastName;
	let uEmail = details.email;

	if(uFirstName !== '' && uLastName !== '' && uEmail !== ''){
		let updatedUser = {
			firstName:uFirstName,
			lastName:uLastName,
			email:uEmail
		}
		return User.findByIdAndUpdate(userId,updatedUser).then((userUpdated,error) => {
			if(error){
				return res.send({Message:"Unable to Delete User"});
			} else {
				return true
			}
		})

	} else {
		res.send({Message:"Please fill up all fields"})
	}
}

// Login User
module.exports.loginUser = (req,res) => {
	//console.log(req.body.email);
	let email = req.body.email
	let password = req.body.password
	User.findOne({email: email}).then(foundUser => {
		console.log(foundUser);
		if(foundUser === null){
			return res.send({Message:"User Not Found"});
		} else {
			const isPasswordCorrect = bcrypt.compareSync(password, foundUser.password)
			console.log(isPasswordCorrect);
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send({Message: "Token Not Created"});
			}
		}
	}).catch(err => res.send(err))
};

// Get User Details
module.exports.getUserDetails = (req,res) => {
	User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
}

// Get All User
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	})
};

// Delete a specific user
module.exports.deleteUser = (id) => {
	return User.findByIdAndRemove(id).then((removedUser, error) => {
		if(error){
			return res.send({Message:"User Not Found unable to Delete"});
		} else {
			return true;
		}
	});
};