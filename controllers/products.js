// [SECTION] Dependencies and Modules
const Product = require('../models/Product.js');
const exp = require('express');

// [SECTION] CREATE

// Create Product - Admin Only
module.exports.createProduct = (req,res) => {
	let newName = req.body.name;
	let newDescription = req.body.description;
	let newPrice = req.body.price;
	let newQuantity = req.body.quantity;

	let newProduct = new Product({
		name:newName,
		description:newDescription,
		price:newPrice,
		quantity:newQuantity
	});

	return Product.findOne({name:newName}).then(foundProduct => {
		if(foundProduct) {
			res.send({Message:`Product ${newName} is already created`});
		} else {
			return newProduct.save().then((success,error) => {
				if(error){
					res.send({Message: "Error: Unable to Save Product"});
				} else {
					res.send(true);
				}
			})
		}
	})
};

module.exports.getAllProducts = (req,res) => {
	return Product.find({}).then(result => {
		res.send(result);
	});
};

module.exports.getProduct = (req,res) => {
	let id = req.params.id;
	return Product.findById(id).then(result => {
		res.send(result);
	});
}


module.exports.updateProducts = (req,res) => {
	let id = req.params.id;
	let upName = req.body.name;
	let upDesc = req.body.description;
	let upPrice = req.body.price;
	let upQuantity = req.body.quantity;

	if(upName !== '' && upDesc !== '' && upPrice !== '' && upQuantity !== ''){
		let updatedProduct = {
			name:upName,
			description:upDesc,
			price:upPrice,
			quantity:upQuantity
		};

		return Product.findByIdAndUpdate(id,updatedProduct).then((success,error) => {
			if(error) {
				res.send({Message: "Error: No product found unable to update"});
			} else {
				res.send(true);
			}
		});

	} else {
		res.send({Message: "All Inputs are Required"});
	}
};

module.exports.deactivateProduct = (req,res) => {
	let id = req.params.id;
	let updates = {
		isAvailable:false
	};
	return Product.findByIdAndUpdate(id,updates).then((success,error) => {
		if (error) {
			res.send({Message:"Error: Unable to Archive Product"});
		} else {
			res.send(true);
		}
	});
};

module.exports.activateProduct = (req,res) => {
	let id = req.params.id;
	let updates = {
		isAvailable: true
	};
	return Product.findByIdAndUpdate(id,updates).then((success,error) => {
		if(error) {
			res.send({Message:"Error: Unable to Activate Product"});
		} else {
			res.send(true);
		}
	});
};