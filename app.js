// [SECTION] Packages and Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');

const userRoutes = require('./routes/users.js');
const productRoutes = require('./routes/products.js');

// [SECTION] Server Setup
const app = express();
dotenv.config();
app.use(cors());
app.use(express.json());
const connString = process.env.CONNECTION_STRING;
const port = process.env.PORT;

// [SECTION] Application Routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);

// [SECTION] Database Connection
mongoose.connect(connString);
let connectionStatus = mongoose.connection;
connectionStatus.on('open', () => console.log(`Database is Connected`));
app.get('/', (req,res) => {
	res.send("Welcome to Techly - You're Source in Every Technology");
});

// [SECTION] Gateway Response
app.listen(port, () => console.log(`Server is running on port ${port}`));