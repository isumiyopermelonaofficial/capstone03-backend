// [SECTION] Dependencies and Modules
const exp = require('express');
const auth = require('../auth.js');
const controller = require('../controllers/products.js');

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const route = exp.Router();

// [SECTION] Post Routes
route.post('/createProduct', verify, verifyAdmin, controller.createProduct);

// [SECTION] Get Routes
route.get('/all', verify, verifyAdmin, controller.getAllProducts);
route.get('/:id', verify, verifyAdmin, controller.getProduct);

// [SECTION] Put Routes
route.put('/:id', verify, verifyAdmin, controller.updateProducts);
route.put('/:id/archive', verify, verifyAdmin, controller.deactivateProduct);
route.put('/:id/activate', verify, verifyAdmin, controller.activateProduct);



// [SECTION] Export Route System
module.exports = route;