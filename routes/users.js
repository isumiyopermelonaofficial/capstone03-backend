// [SECTION] Dependencies and Modules
const exp = require('express');
const res = require('express/lib/response');
const controller = require('../controllers/users.js');
const auth = require('../auth.js');
const jwt = require('jsonwebtoken');

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const route = exp.Router();

//[SECTION] POST ROUTES
route.post('/register', controller.registerUser);
route.post('/login', controller.loginUser);

// [SECTION] PUT Routes
// update user information
route.put('/:id', verify, verifyAdmin, controller.updateUser)

// [SECTION] GET ROUTES
route.get('/getUserDetails', verify, controller.getUserDetails);

route.get('/all', verify, verifyAdmin, (req,res) => {
    controller.getAllUsers().then(result => {
        res.send(result);
    });
});

// [SECTION] DEL ROUTES
route.delete('/:id',verify, verifyAdmin, (req,res) => {
	let userId = req.params.id;
	controller.deleteUser(userId).then(result => {
		res.send(result);
	});
});





// [SECTION] Export Route System
module.exports = route;