// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Product Name is Required']
	},

	description: {
		type: String,
		required: [true, 'Product Description is Required']
	},

	price: {
		type: Number,
		required: [true, 'Product Price os Required']
	},

	quantity: {
		type: Number,
		required: [true, 'Product Quantity is Required']
	},

	isAvailable: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
});

// [SECTION] Model
const Product = mongoose.model('Product', productSchema);
module.exports = Product