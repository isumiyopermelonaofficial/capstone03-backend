// [SECTION] Dependencies and Modules
const mongoose = require('mongoose');

// [SECTION] Blueprint Schema
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	
	userOrders: [
	{
		orderId: {
			type: String,
			required: [true, 'OrderID is Required']
		},
		purchasedOn: {
			type: Date,
			required: new Date() 
		}

	}
	]
});

// [SECTION] Model
const User = mongoose.model('User', userSchema);
module.exports = User;